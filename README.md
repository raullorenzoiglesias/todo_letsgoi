## Instrucciones para lanzar la aplicacion

Descargar la ultima version de node:  

https://nodejs.org/en/

Desde linea de comandos, ubicarse en la carpeta del proyecto y lanzar:  

npm start

NOTA: Si utilizas windows y git bash para lanzar el proceso, el usar ctrl + c no para el servidor. Para hacerlo debemos listar los procesos con sus puertos: 

netstat -nqo

Localizamos el proceso en el puerto que se lanzo (por defecto 3000), apuntamos el PID:

 TCP    0.0.0.0:3000           0.0.0.0:0              LISTENING       3516

Matamos el proceso con un sigkill:

tskill {PID}

## Aplicacion todo con ReactJS y Sass

La estructura de la aplicacion se ha realizaon con react-create-app.

La aplicacion se encuentra en la carpeta src.


## Arquitectura

La aplicacion se ha creado siguiendo arquitectura FLUX:

- Tenemos un componente TodoStore, que se encarga de obtener los datos de back end y modelarlos.
- El componente principal usa store para acceder a los datos.
- Para modificar la store se usa un dispatcher a traves de TodoActions, mediante un patron pub/sub, donde nuestra store, se suscribe a las acciones que definamos, que luego son usadas por el resto de componentes.

Componentes:

- App: Componente principal que contiene la aplicacion, simplemente lista las tareas que obtiene de la store.
- TodoTask: Componente que renderiza cada tarea, y ademas nos permite borrarlas.
- TodoAddTask: Componente que nos permite añadir tareas.
- TodoFilter: Compoennte que nos permite filtrar la lista de tareas.
- Dispatcher: Componente que nos permite aplicar el patron pub/sub.
- TodoActions: Componente que contiene las acciones para publicar a traves del dispatcher.
- TodoStore: Componente Store que obtiene la informacion de back y la modela, usa el dispatcher para suscribirse a las distintas acciones que hemos definido.
