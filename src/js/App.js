import React, { Component } from 'react';

//Imgs
import logo from '../img/logo.svg';

//CSS
import '../css/todoApp.css';

//Components
import TodoAddTask from './TodoAddTask';
import TodoFilter from './TodoFilter';
import TodoTask from './TodoTask';
import TodoStore from './store/TodoStore';
import * as TodoActions from './store/TodoActions';

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos: TodoStore.getAll()
    }
    this.loadTodos();
  }
  /* 
  Metodo que se lanza cuando se monta el componente por primera vez.
  Cada vez que se modifique la tienda, se lanza el evento this.getTodos
   */
  componentWillMount() {
    TodoStore.on('change', this.getTodos.bind(this));
  }

  /* 
  Metodo que se lanza cuando se desmonta el componente por primera vez.
  Eliminamos el listener para evitar leaks
   */
  componentWillUnmount() {
    TodoStore.removeListener('change', this.getTodos.bind(this));
  }

  /* 
  Obtiene de la store la lista de todos
   */
  getTodos() {
    this.setState({
      todos: TodoStore.getAll()
    });
  }

  /* 
  Ordena a la store que se actualice con back
   */
  loadTodos() {
    TodoActions.loadTodos();
  }

  render() {
    //Obtenemos la lista de todos, y los tranformamos en componentes
    const { todos } = this.state;
    const TodoList = todos.map((todo) => {
      return <TodoTask key={todo.id} {...todo} />
    })
    
    return (
      <div className="wrapper">
        <header>
          <img src={logo} className="logoReact" alt="logo" />
          <h1 className="headerTitle">Aplicacion Todo</h1>
        </header>
        <section className="todoApp">
          <section className="todoActions">
            <TodoFilter />
            <TodoAddTask />
          </section>
          <section className="todoList">
            <ul>{TodoList} </ul>
          </section>
        </section>
      </div>
    );
  }
}

export default App;
