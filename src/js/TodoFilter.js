import React, { Component } from 'react';
import * as TodoActions from './store/TodoActions';

class TodoFilter extends Component {

  /* 
  Lanza la accion para actualizar el valor del filtro de la store
  */
  updateFilter(event){
    TodoActions.updateFilter(event.target.value)
  }

  render() {
    return (
      <div className="todoFilter">
        <h2>Filtrar tareas</h2>
        <div>
          <span>
            <h4>Palabra</h4>
            <input  className="todoWordFilter" onChange={this.updateFilter.bind(this)} />
          </span>
        </div>
      </div>
    );
  }
}

export default TodoFilter;
