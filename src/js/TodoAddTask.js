import React, { Component } from 'react';
import * as TodoActions from './store/TodoActions';

class TodoAddTask extends Component {
  constructor(){
    super();
    this.state = {
      userId: '',
      id: Date.now(),
      title: '',
      completed: false
    }
  }

  /* 
  Actualiza el valor de userId cada vez que se modifica el input de Id usuario
  */
  changeUserId(event){
    const userId = event.target.value;
    this.setState({userId: userId});
  }

  /* 
  Actualiza el valor de title cada vez que se modifica el input de Tarea
  */
  changeTitle(event){
    const title = event.target.value;
    this.setState({title: title});
  }

  /* 
  Lanza la accion para crear tareas y reseta los inputs
  */
  addTask(){
    TodoActions.createTodo(this.state);
    this.setState({
      userId: '',
      id: Date.now(),
      title: '',
      completed: false
    });
  }

  render() {
    return (
      <div className="todoAddTask">
        <h2>Añadir tarea</h2>
        <div>
          <span>
            <h4>Id usuario</h4>
            <input  className="todoInputUserId" onChange={this.changeUserId.bind(this)} value={this.state.userId}/>
          </span>
          <span>
            <h4>Tarea</h4>
            <input className="todoInputTitle" onChange={this.changeTitle.bind(this)} value={this.state.title}/>
          </span>
        </div>
        <button className="todoAddTaskButton" onClick={this.addTask.bind(this)}>Añadir</button>
      </div>
    );
  }
}

export default TodoAddTask;
