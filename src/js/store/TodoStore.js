import { EventEmitter } from 'events';

import Dispatcher from './Dispatcher';

class TodoStore extends EventEmitter {
  constructor() {
    super();
    this.todos = [
      {
        "userId": 1,
        "id": 1,
        "title": "delectus aut autem",
        "completed": false
      },
      {
        "userId": 1,
        "id": 2,
        "title": "quis ut nam facilis et officia qui",
        "completed": false
      },
      {
        "userId": 1,
        "id": 3,
        "title": "fugiat veniam minus",
        "completed": false
      },
      {
        "userId": 1,
        "id": 4,
        "title": "et porro tempora",
        "completed": true
      },
      {
        "userId": 1,
        "id": 5,
        "title": "laboriosam mollitia et enim quasi adipisci quia provident illum",
        "completed": false
      },
      {
        "userId": 1,
        "id": 6,
        "title": "qui ullam ratione quibusdam voluptatem quia omnis",
        "completed": false
      },
      {
        "userId": 1,
        "id": 7,
        "title": "illo expedita consequatur quia in",
        "completed": false
      },
      {
        "userId": 1,
        "id": 8,
        "title": "quo adipisci enim quam ut ab",
        "completed": true
      }
    ];
    this.filter = '';
  }

  /* 
  Crea una nueva tarea
  */
  createTodo(params) {
    this.todos.push(params);
    this.emit('change');
  }

  /* 
  Crea una tarea
  */
  deleteTodo(id) {
    this.todos = this.todos.filter((todo) => { return todo.id !== id });
    this.emit('change');
  }

  /* 
  Recarga la lista de tareas con la lista que recibe de back
  */
  loadTodos(todos) {
    this.todos = todos;
    this.emit('change');
  }

  /* 
  Actualiza el valor del filtro
  */
  updateFilter(filter){
    this.filter = filter;
    this.emit('change');
    
  }

  /* 
  Devuelve la lista de tareas a mostrar debidamente filtrada.
  */
  getAll() {
    return this.todos.filter((todo) => todo.title.includes(this.filter));
  }

  /* 
  Handler de las acciones que lanza el dispatcher.
  Aqui listamos las acciones a las que esta suscrita la store.
  */
  handleActions(action) {
    switch (action.type) {
      case 'CREATE_TODO':
        this.createTodo(action.params);
        break;
      case 'LOAD_TODOS':
        this.loadTodos(action.todos);
        break;
      case 'DELETE_TODO':
        this.deleteTodo(action.id);
        break;
      case 'UPDATE_FILTER':
        this.updateFilter(action.filter);
        break;
      default:
        break;
    }
  }
}

const todoStore = new TodoStore();
Dispatcher.register(todoStore.handleActions.bind(todoStore));
export default todoStore;