import Dispatcher from './Dispatcher';
import axios from 'axios';

export function createTodo(params) {
  Dispatcher.dispatch({
    type: 'CREATE_TODO',
    params
  });
}

export function deleteTodo(id) {
  Dispatcher.dispatch({
    type: 'DELETE_TODO',
    id
  });
}

export function loadTodos() {
  axios.get('https://jsonplaceholder.typicode.com/todos').then((response)=>{
      Dispatcher.dispatch({
        type: 'LOAD_TODOS',
        todos: response.data
      });   
    });
}

export function updateFilter(filter) {
  Dispatcher.dispatch({
    type: 'UPDATE_FILTER',
    filter
  });
}