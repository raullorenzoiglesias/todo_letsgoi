import React, { Component } from 'react';
import * as TodoActions from './store/TodoActions';

class TodoTask extends Component {
  constructor(props){
    super(props);
    this.state = {
      userId: this.props.userId,
      title: this.props.title,
      completed: this.props.completed
    } 
  }

  deleteTask(){
    TodoActions.deleteTodo(this.props.id);
  }

  updateCheck(){

    this.setState({
      completed : !this.state.completed
    });
  }

  render() {
    const {title, completed} = this.props;
    return (
      <li className="todoTask">
        <span className="todoUser">
        <input type="checkbox" onChange={this.updateCheck.bind(this)} defaultChecked={completed}/>
          Usuario: {this.state.userId}</span>
        <span className="todoTitle">{title}</span>
        <button className="todoTaskDeleteButton" onClick={this.deleteTask.bind(this)}>Borrar tarea</button>
      </li>
    );
  }
}

export default TodoTask;
