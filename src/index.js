//Imports basicos react
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

//Aplicacion
import App from './js/App';


ReactDOM.render(<App />, document.getElementById('app'));
registerServiceWorker();
